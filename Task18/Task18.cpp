﻿#include <iostream>

class Player
{
public:
    int score = 0;
    std::string name;

    int ReturnScore()
    {
        return score;
    }
    std::string ReturnName()
    {
        return name;
    }
};

void quicksort(Player* arr, int l, int r) {
    if (r - l <= 1) return;
    int z = (l + (r - l) / 2);
    int ll = l, rr = r - 1;

    while (ll <= rr) {
        while (arr[ll].score < arr[z].score) ll++;
        while (arr[rr].score > arr[z].score) rr--;
        if (ll <= rr) {
            std::swap(arr[ll], arr[rr]);
            ll++;
            rr--;
        }
    }
    if (l < rr) quicksort(arr, l, rr + 1);
    if (ll < r) quicksort(arr, ll, r);
}


int main()
{
    int num;
    Player a;
    std::cout << "How many players:\n";
    std::cin >> num;
    Player* plarr = new Player[num];
    for (int i = 0; i < num; i++)
    {
        std::cout << "Enter " << i << " player name: \n";
        std::cin >> a.name;
        std::cout << "Enter " << i << " player score: \n";
        std::cin >> a.score;
        plarr[i] = a;
        //std::cout << "value of " << i << " is " << plarr[i].score;
    }

    quicksort(plarr, 0, num);

    for (int i = 0; i < num; i++)
    {
        std::cout << i << " place: " << plarr[i].name << " with " << plarr[i].score << " scores\n";
    }



    delete[] plarr;
    return 0;
}