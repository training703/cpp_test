﻿
#include <time.h>
#include <iostream>

int main()
{
    const int N = 10;
    int sum = 0;
    int array[N][N];
    bool IsFound=0;


    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
  

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            if (i == buf.tm_mday % N)
            {
                sum = sum + array[i][j];
                IsFound = 1;
            }
            std::cout << array[i][j]<<" ";
        }

        //Если нашли сумму, то пишем её в текущую строку
        if (IsFound==1)
        {
            std::cout << sum;
            IsFound = 0;
        }
        std::cout << "\n";
    }
    return 0;
}

