﻿#include <iostream>
#include <cmath>
class Vector
{
private:
    double x, y, z;
public:
    Vector():x(0), y(0),z(0)
    {}
    Vector(double _x, double _y, double _z):x(_x),y(_y),z(_z)
    {}
    void Show()
    {
        std::cout << "\n" << x << ' ' << y << ' ' << z << "\n";
    }
    double Mod()
    {
        return sqrt(x*x+y*y+z*z);
    }
};

int main()
{   
    double x, y, z;
    std::cout << "Enter x" << "\n";
    std::cin >> x;
    std::cout << "Enter y" << "\n";
    std::cin >> y;
    std::cout << "Enter z" << "\n";
    std::cin >> z;


    Vector v(x,y,z);
    v.Show();
    std::cout<<"Vectors length: " << v.Mod();

    return 0;
}

