﻿#include <iostream>
#include <stdlib.h>
#include <time.h>  

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "!\n";
    }

};

class Dog : public Animal
{
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
    void Voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Frog : public Animal
{
    void Voice() override
    {
        std::cout << "Ribbit!\n";
    }
};

class Cow : public Animal
{
    void Voice() override
    {
        std::cout << "Moo!\n";
    }
};

class Pig : public Animal
{
    void Voice() override
    {
        std::cout << "Oink!\n";
    }
};

int main()
{
    const int length = 5;
    Animal* animals[length]{};

    for (int i = 0; i < length; i++)
    {
        int r = rand()%5;
        switch (r)
        {
        case 0: animals[i] = new Dog;
            break;
        case 1: animals[i] = new Cat;
            break;
        case 2: animals[i] = new Frog;
            break;
        case 3: animals[i] = new Cow;
            break;
        case 4: animals[i] = new Pig;
            break;
          
        }
    }

    for (int i = 0; i < length; i++)
    {
     animals[i]->Voice();
    }

    return 0;
}
