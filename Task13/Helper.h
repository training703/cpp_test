int quadsum(int a, int b)
{
	return a ^ 2 + (2 * a * b) + b ^ 2;
}
